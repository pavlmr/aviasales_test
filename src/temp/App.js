import React, { Component } from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import rootReducer from './reducers';

import { Layout } from './components';

const store = createStore(
	rootReducer,
	composeWithDevTools(applyMiddleware(thunk))
);

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<Layout />
			</Provider>
		);
	}
}


export default App