import { LOAD_TICKETS } from '../types';
import { getApiData, ticketsInnerUrl} from '../helpers';    

export const getTickets = (dispatch) => async () => {
    const payload = await getApiData(ticketsInnerUrl)

    payload && dispatch({
        type: LOAD_TICKETS,
        payload
    })
    return payload
}