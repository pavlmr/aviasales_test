import React from 'react';
import { Tickets } from './';

const Layout = () => (
    <div className="cover pb-5-rem justify-start">
        <div className="cont justify-center">
            <div className="col-auto logo"><img src="assets/images/logo.png" alt="logo" /></div>
        </div>
        <Tickets />
    </div>
)
export default Layout;