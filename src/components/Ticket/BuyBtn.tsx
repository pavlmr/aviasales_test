import React from 'react';
import { iBuyBtn, iIconCurrency } from '../../interfaces'

const Icon = ({ currency }: iIconCurrency) => {
    if (currency === "USD") return <i className="fas fa-dollar-sign"></i>
    if (currency === "EUR") return <i className="fas fa-euro-sign"></i>
    if (currency === "RUB") return <i className="fas fa-ruble-sign"></i>
    return <i className="fas fa-ruble-sign"></i>
}

const BuyBtn = ({ price, currency, rateData }: iBuyBtn) => {
    const ratedPrice: number = Math.floor(price * rateData[currency]);
    return(
        <div className="btn btn_second buy-btn"><span>Купить<br /> за {ratedPrice} <Icon currency={currency}/></span></div>
    )
}

export default BuyBtn