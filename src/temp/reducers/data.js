import {
	LOAD_TICKETS,
} from '../types';

// import { contentMapper, settingsMapper, mainPageMapper } from '../helpers';

const initialState = {
	tickets: {},
};

export default (state = initialState, { type, payload }) => {
	switch (type) {

		case LOAD_TICKETS:
			return {
				...state,
				tickets: payload.tickets
            };
            
		default:
			return state;
	}
};