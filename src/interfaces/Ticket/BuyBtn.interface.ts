export default interface iBuyBtn {
    price: number,
    currency: string,
    rateData: {
        [index: string]: number
    }
}