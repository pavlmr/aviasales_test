import React from 'react';
import { Ticket } from './Ticket';
import { PreLoader } from '../helpers';

import { iTicketsList } from '../interfaces';

const TicketsList = ({ tickets, currency, rateData, carrierLogoUrls }: iTicketsList) => {
    if (!tickets) {
        return(
            <PreLoader />
        )
    }
    return(
        <React.Fragment>
            {tickets.map((ticket, key) => {
                return <Ticket {...ticket} currency={currency} rateData={rateData} carrierLogoUrls={carrierLogoUrls} key={key} />
            })}
        </React.Fragment>
    )
}

export default TicketsList;

