export const stopsData = [
    {
        label: 'Все',
        id: 'sq_all',
        value: 'All',
        checked: true
    },
    {
        label: 'Без пересадок',
        id: 'sq_0',
        value: 0,
        checked: true
    },
    {
        label: '1 пересадка',
        id: 'sq_1',
        value: 1,
        checked: true
    },
    {
        label: '2 пересадки',
        id: 'sq_2',
        value: 2,
        checked: true
    },
    {
        label: '3 пересадки',
        id: 'sq_3',
        value: 3,
        checked: true
    }
]

export const currencyData = [
    {
        label: 'RUB',
        id: 'cur_rub',
        defaultChecked: true
    },
    {
        label: 'USD',
        id: 'cur_usd',
        defaultChecked: false
    },
    {
        label: 'EUR',
        id: 'cur_eur',
        defaultChecked: false
    },
]

export const rateData = {
    'RUB': 1,
    'USD': 0.0155617802676626,
    'EUR': 0.0138927479855515
}

export const carrierLogoUrls = {
    'SU': 'assets/images/su_logo.png',
    'S7': 'assets/images/s7_logo.png',
    'TK': 'assets/images/ta_logo.png',
    'BA': 'assets/images/ba_logo.png'
}