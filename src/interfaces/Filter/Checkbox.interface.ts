export default interface iCheckbox {
    label: string,
    id: string,
    value: string | number,
    checked: boolean,
    changeStops: (value: string, checked: boolean, only?: boolean) => void
}
