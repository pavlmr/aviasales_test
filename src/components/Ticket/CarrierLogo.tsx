import React from 'react';
import { iCarrierLogo } from '../../interfaces'

const CarrierLogo = ({ carrier, carrierLogoUrls }: iCarrierLogo) => (
    <img src={carrierLogoUrls[carrier]} alt={carrier} />
)

export default CarrierLogo