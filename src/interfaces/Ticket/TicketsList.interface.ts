export default interface iTicketsList {
    tickets: never[],
    currency: string,
    rateData: {
        [index: string]: number
    }
    carrierLogoUrls: {
        [index: string]: string
    }
}