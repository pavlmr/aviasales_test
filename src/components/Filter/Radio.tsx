import React from 'react';
import {iRadio} from '../../interfaces';

const Radio = ({ label, id, defaultChecked, setCurrency }: iRadio) => (
    <div className="col-8 compans field boolean f-ccc radio">
        <input id={id} type="radio" name="currency" value={label} defaultChecked={defaultChecked} onChange={e => setCurrency(e.target.value)} />
        <label htmlFor={id}><span>{label}</span></label>
    </div>
);

export default Radio;