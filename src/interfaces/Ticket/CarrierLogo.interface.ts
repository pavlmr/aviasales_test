export default interface iCarrierLogo {
    carrier: string,
    carrierLogoUrls: {
        [index: string]: string
    }
}