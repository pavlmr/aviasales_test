import React from 'react';
import { useState, useEffect } from 'react';
import { Filter } from './Filter';
import { TicketsList } from './';
import { getApiData, ticketsInnerUrl, sortByPrice, isAllChecked, writeCheckedStatus } from '../helpers';
import { stopsData, currencyData, rateData, carrierLogoUrls } from '../data';
import { iFilterStopsData } from '../interfaces';

// import { TicketsDataContext } from '../context'

const Tickets = () => {
    const [tickets, setTickets] = useState([]);
    const [filterStopsData, setFilterStopsData] = useState<iFilterStopsData[]>([]);
    const [currency, setCurrency] = useState('RUB');
    const [stops, setStops] = useState<{}[]>([]);

    const changeStops = (value:string|number, status:boolean, only?: boolean):boolean => {

        let FSD:iFilterStopsData[] = [...filterStopsData];
        let stopsChecked:any[] = [];

        // Если первый флажек(Все)
        if (value === 'All') {
            // Отобразить/Скрыть Все билеты
            stopsChecked = status ? [0, 1, 2, 3] : ['Hide'];

            // Простановка статусов флажков
            writeCheckedStatus(FSD, status);

            // Запись данных в State
            setFilterStopsData(FSD)
            setStops(stopsChecked)

            return true;
        }
        
        // Поиск индекс текущего флажка по Value
        const index = filterStopsData.findIndex((element:{value: string|number}) => element.value === Number(value));

        // Установка статус текушего флажка
        FSD[index].checked = status;

        // Изменение первого флажка(Все) в заисимости от статуса
        status ? isAllChecked(FSD, true) && (FSD[0].checked = true) : (FSD[0].checked = false);

        (only && status) && writeCheckedStatus(FSD, status, value, 'FIRST_BY_STATUS');

        // Сбор номеров включеных флажков в массив
        FSD.map((item:{checked:boolean, value: string|number}) => (item.checked && item.value !== 'All') && stopsChecked.push(item.value));
        (stopsChecked.length === 0) && (stopsChecked = ['Hide']);

        // Запись данные в State
        setFilterStopsData(FSD)
        setStops(stopsChecked)

        return true;
    }

    const getTickets = async () => {
        let res: { tickets?: [] } = await getApiData(ticketsInnerUrl);
        let ticketsData = res.tickets;
        if (ticketsData !== undefined) {
            sortByPrice(ticketsData);
            setTickets(ticketsData);
        }
    }

    const getStops = () => {
        // ToDo: Автоматически составлять набор фильтров иcходя из данных по билетам.
        setFilterStopsData((stopsData))
    }

    const getCheckedStatus = () => {
        let stopsChecked: any[] = [];
        stopsData.map( (item: { checked: boolean, value: string | number }) =>(item.checked && item.value !== 1234) && stopsChecked.push(item.value))
        setStops(stopsChecked)
    }

    useEffect(() => {
        getTickets();
        getStops();
        getCheckedStatus();
    }, [])

    //Фильтрация 
    const filterByStops = (ticket:{stops:never}) => stops.includes(ticket.stops);
    const filterTicketsData = (stops.length !== 0) ? tickets.filter(filterByStops) : tickets;

    // let context = {
    //     ticketsData: tickets,
    //     stopsData: stops,
    //     currencyData,
    //     filterStopsData
    // }

    return(
        // <TicketsDataContext.Provider value={context}>
            <section className="cont justify-center">
                <aside className="col-24 col-lg-8 col-xl-6">
                    <Filter changeStops={changeStops} setCurrency={setCurrency} filterStopsData={filterStopsData} currencyData={currencyData} />
                </aside>
                <ul className="col-24 col-lg-16 col-xl-14 tickets">
                    <TicketsList tickets={filterTicketsData} currency={currency} rateData={rateData} carrierLogoUrls={carrierLogoUrls}/>
                </ul>
            </section>
        // </TicketsDataContext.Provider>
    )
}

export default Tickets;
