import Ticket from './Ticket';
import BuyBtn from './BuyBtn';
import CarrierLogo from './CarrierLogo'

export{
    Ticket,
    BuyBtn,
    CarrierLogo
}