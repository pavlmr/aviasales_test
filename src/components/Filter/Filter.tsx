import React from 'react';
import Checkbox from './Checkbox';
import Radio from './Radio';

import { iFilter } from '../../interfaces';

const Filter = ({ changeStops, setCurrency, filterStopsData, currencyData }: iFilter) => (
    <form className="col-24 compans filter column" action="/">
        <fieldset className="col compans currency">
            <div className="legend">Валюта</div>
            <div className="col row f-nowrap radio-groupe">
                {currencyData.map(item => <Radio {...item} setCurrency={setCurrency} key={item.id} />)}
            </div>
        </fieldset>
        <fieldset className="col compans stop-quantity column items-stretch content-stretch" >
            <div className="legend">Количество пересадок</div>
            {filterStopsData.map(item => <Checkbox {...item} changeStops={changeStops} key={item.id} />)}
        </fieldset>
    </form>
)


export default Filter;
