export default interface iRadio {
    label: string,
    id: string,
    defaultChecked: boolean,
    setCurrency: (value: string) => void
}