import iFilterStopsData from './StopsData.interface'
export default interface Filter {
    changeStops: (value: string | number, status: boolean, only?: boolean) => boolean,
    setCurrency: (value: string) => void,
    filterStopsData: iFilterStopsData[],
    currencyData: {
        label: string,
        id: string,
        defaultChecked: boolean
    }[]
}
