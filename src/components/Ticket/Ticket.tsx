import React from 'react';
import { stopsText } from '../../helpers';
import moment from 'moment';
import 'moment/locale/ru'; //ToDo: add a new declaration
import { iTicket } from '../../interfaces'
import { BuyBtn, CarrierLogo } from './';
// import { carrierLogoUrls } from '../../data';

const Ticket = ({ carrier, price, departure_time, stops, arrival_time, origin, origin_name, departure_date, destination, destination_name, arrival_date, currency, rateData, carrierLogoUrls }: iTicket) => (
    <li className="col-24 compans ticket">

        <div className="col-24 col-md-9 col-lg-8 col-xl-9 column items-stretch content-stretch ticket__action">
            <div className="col-auto justify-center ticket__carrier">
                <CarrierLogo carrier={carrier} carrierLogoUrls={carrierLogoUrls} />
            </div>
            <div className="col-auto column items-stretch content-stretch">
                <BuyBtn price={price} currency={currency} rateData={rateData}/>
            </div>
        </div>

        <div className="col-24 col-md-15 col-lg-16 col-xl-15 ticket__info">
            <div className="col-24 items-center">
                <div className="col-auto compans ticket__info__time">{departure_time}</div>
                <div className="col-auto grow-1 compans column">
                    <div className="col compans justify-center items-center ticket__info__stops">
                        {stopsText(stops)}
                    </div>
                    <div className="col compans justify-center items-center">
                        <div className="ticket__info__line"></div>
                        <img src="assets/images/airplane.png" alt="airplane.png" />
                    </div>
                </div>
                <div className="col-auto compans  ticket__info__time">{arrival_time}</div>
            </div>
            <div className="col-24">
                <div className="col-auto compans column">
                    <span className="ticket__info__name">{origin}, {origin_name}</span>
                    <time dateTime={moment(departure_date, "MM-DD-YYYY").format('YYYY-MM-DD') + 'T' + departure_time} className="ticket__info__date">{moment(departure_date, "MM-DD-YYYY").locale('ru').format('D MMM Y, ddd')}</time>
                </div>
                <div className="col-auto grow-1 compans justify-center"></div>
                <div className="col-auto compans ta-r items-end column">
                    <span className="ticket__info__name">{destination}, {destination_name}</span>
                    <time dateTime={moment(arrival_date, "MM-DD-YYYY").format('YYYY-MM-DD') + 'T' + arrival_time} className="ticket__info__date">{moment(arrival_date, "MM-DD-YYYY").locale('ru').format('D MMM Y, ddd')}</time>
                </div>
            </div>
        </div>

    </li>
)

export default Ticket;
