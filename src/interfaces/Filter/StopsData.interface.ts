export default interface iFilterStopsData {
    id: string,
    label: string,
    value: string | number,
    checked: boolean
}