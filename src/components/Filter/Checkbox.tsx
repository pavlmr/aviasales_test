import React from 'react';
import { iCheckbox } from '../../interfaces';

const Checkbox = ({ label, id, value, checked, changeStops }: iCheckbox) => (
	<div className="field stops boolean items-stretch content-stretch">
		<input id={id} name={id} type="checkbox" checked={checked} value={value} onChange={e => changeStops(e.target.value, e.target.checked)} />
		<label htmlFor={id}>{label}</label>
		{(id !== 'sq_all') && <div className="checkbox__only-btn items-center" data-checked="only" onClick={() => changeStops(value.toString(), true, true)}><span>только</span></div>}
	</div>
)

export default Checkbox