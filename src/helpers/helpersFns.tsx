import React from 'react';
import { apiUrl } from './consts';
import { iFilterStopsData } from '../interfaces';

export const getApiData = ( innerUrl:string = '', options?: object ): object =>
    fetch( apiUrl + innerUrl, options )
        .then( response => response.json() )
        .catch( err => console.warn('Error on api fetching', err) );

export const PreLoader = () => (
    <div className = "col-24 compans preloader justify-center">Preloader</div>
)

export const stopsText = (stops: number): string => {
    if (stops === 0) return 'Без пересадок';
    if (stops === 1) return '1 пересадка';
    return `${stops} пересадки`;
}

export const sortByPrice = (item: { price: number }[]) => {
    item.sort((a, b) => {
        if (a.price > b.price) {
            return 1;
        }
        if (a.price < b.price) {
            return -1;
        }
        return 0;
    })
}

export const isAllChecked = (array: { checked: boolean }[], boolean: boolean): boolean => {
    let status = boolean
    let i = array.length - 1;

    while (i > 0) {
        !array[i].checked && (status = !boolean)
        i--
    }

    return status;
}

export const writeCheckedStatus = (inputs: iFilterStopsData[], newStatus: boolean, value?: number | string, method?: string) => {
    switch (method) {
        case 'FIRST_BY_STATUS': {
            let i = inputs.length - 1;
            while (i > -1) {
                (value !== (i - 1).toString()) && (inputs[i].checked = false)
                --i
            }
        } break;
        
        default: {
            let i = inputs.length - 1;
            while (i > -1) {
                inputs[i].checked = newStatus;
                i--
            }
        } break;
    }
}